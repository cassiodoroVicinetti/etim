\chapter{Tecniche parametriche}
Lo sviluppo delle \textbf{tecniche parametriche} iniziò negli anni `70 con la necessità da parte del dipartimento della difesa statunitense di creare una rete di telefonia digitale criptata da usare in ambito militare su cui trasmettere la voce attraverso i modem dell'epoca, che avevano velocità di trasmissione (2400 b/s) assai più basse dei bit rate richiesti dalle tecniche PCM (ogni campione richiederebbe di essere codificato in frazioni di bit!).

Anche la nascente telefonia cellulare digitale a partire dall'inizio degli anni '80 cerca una codifica per la voce digitale a bit rate più bassi:
\begin{itemize}
\item qualità: toll quality
\item bit rate: $\approx$ 12 kb/s
\end{itemize}

Le tecniche parametriche descrivono il segnale vocale inteso come contenuto, non come forma d'onda: viene creato un modello di produzione del sistema fonatorio umano, chiamato \textbf{modello di produzione} del segnale vocale, e vengono trasmessi solo i parametri di questo modello.

\section{Codifica predittiva lineare: LPC-10}
\begin{figure}
	\centering
	\includegraphics[width=0.8\linewidth]{pic/A5/Voice_production_model_signals}
	\caption{Produzione del segnale vocale attraverso il sistema fonatorio umano.}
\end{figure}

\noindent
Lo standard \textbf{LPC-10} (Linear Predictive Coding), sviluppato negli anni `70, fu il primo standard NATO utilizzato per la telefonia digitale criptata per usi militari e diplomatici:
\begin{itemize}
\item[\pro] \ul{bit rate}: solo 650 parametri al secondo (13 parametri ogni 20 ms) vengono trasmessi (contro gli 8000 campioni al secondo delle tecniche PCM) $\Rightarrow$ il bit rate si abbassa notevolmente:
\[
R = 60 \; \text{b} \times 50 \; \text{volte/s} = 3000 \; \text{b/s} \simeq 2400 \; \text{b/s}
\]
\item[\pro] \ul{intelligibilità}: è garantita;
\item[\con] \ul{naturalezza}: MOS $\approx$ 2,7: è medio-bassa e al di sotto della toll quality, ma comunque sufficiente per applicazioni militari e satellitari.
\end{itemize}
\FloatBarrier

\subsection{Polmoni}
Dai polmoni esce un flusso d'aria turbolento, che a livello di segnale produce rumore bianco (avente uno spettro piatto).

\paragraph{Parametri}
\begin{itemize}
\item \textbf{gain} (dB, 7 bit): indica l'intensità del rumore bianco
\end{itemize}

\subsection{Laringe}
La laringe può produrre due tipi di suoni:
\begin{itemize}
\item \ul{suoni non vocalizzati} (ad es. consonanti ``s'' e ``f'', bisbiglio): la laringe è aperta $\Rightarrow$ il flusso d'aria turbolento in arrivo dai polmoni non viene modificato, e continua a produrre rumore bianco;
\item \ul{suoni vocalizzati} (ad es. vocali): la laringe si apre e si chiude periodicamente producendo una sequenza di sbuffi (segnale glottale) $\Rightarrow$ le membrane ostruiscono il passaggio dell'aria, e viene prodotto un suono dallo spettro armonico.
\end{itemize}

\paragraph{Parametri}
\begin{itemize}
\item \textbf{flag} (booleano, 1 bit): specifica se il suono è vocalizzato o non vocalizzato
\item \textbf{frequenza fondamentale} (Hz, 7 bit): nel caso vocalizzato, lo spettro armonico del segnale presenta dei picchi periodici in corrispondenza dei multipli della frequenza fondamentale $F_0$ (quindi a $F_0$, a $2 F_0$, ecc.)
\end{itemize}

\subsubsection{Frequenza fondamentale}
La \textbf{frequenza} (o pitch) \textbf{fondamentale} $F_0$ corrisponde all'altezza (nel senso musicale) della voce:
\begin{itemize}
\item è una funzione a tratti: esiste solo quando il suono è vocalizzato;
\item è variabile nel tempo: la prosodia studia la variabilità della frequenza fondamentale in un discorso.
\end{itemize}

La funzione fondamentale è inutile ai fini dell'intelligibilità (un suono non vocalizzato è comunque intelligibile), ma fornisce informazioni importanti per la \ul{naturalezza}:
\begin{itemize}
\item \ul{componenti linguistiche}:
\begin{itemize}
\item segnali linguistici (ad es. intonazione delle domande);
\item arricchimento semantico della frase (ad es. evidenziare parole o fare parentesi);
\end{itemize}

\item \ul{componenti personali}:
\begin{itemize}
\item identità del parlatore (sesso, età\textellipsis): è legata alla frequenza fondamentale media (valori tipici: 100 Hz per gli uomini, 200 Hz per le donne);
\item accento e inflessione dialettale: danno un'idea della provenienza geografica;
\item stato d'animo e di salute del parlatore.
\end{itemize}
\end{itemize}

\subsection{Cavità orale}
\begin{figure}
	\centering
	\includegraphics[width=0.4\linewidth]{pic/A5/Phoneme_chart_example}
	\caption{Grafico dei fonemi: i suoni riconosciuti come lo stesso fonema per una certa lingua sono detti \textbf{suoni allofoni}.}
\end{figure}

\noindent
Il tratto vocale è una cavità complicata: la voce si propaga sia attraverso la cavità nasale sia attraverso quella orale. Per semplicità, si considera la propagazione della voce solamente attraverso la cavità orale.

La cavità orale è la componente responsabile dell'\ul{intelligibilità} attraverso l'emissione di fonemi che verranno poi interpretati dal sistema uditivo umano: il sistema uditivo identifica i fonemi grazie all'analisi dello spettro del segnale, e in particolare delle risonanze (effettua una trasformata di Fourier del suono).

La cavità orale può essere vista come un sistema tempo-variante composto da un ``tubo'' a sezione variabile a causa del diverso posizionamento degli articolatori, in primo luogo della lingua che ``sposta'' le risonanze, e questi articolatori, svariate volte al secondo, enfatizzano alcune frequenze nello spettro del segnale che attraversa la cavità provocando la nascita di \textbf{risonanze} tempo-varianti (quindi non periodiche).

È detta \textbf{formante} la frequenza che viene maggiormente enfatizzata per formare una risonanza. Per la modellizzazione della cavità orale occorre individuare le formanti:
\begin{itemize}
\item \ul{suoni non vocalizzati}: le risonanze agiscono su un segnale piatto, producendo un segnale con dei picchi in frequenza $\Rightarrow$ quei picchi sono proprio le formanti;
\item \ul{suoni vocalizzati}: il segnale in arrivo dalla laringe ha già dei picchi dello spettro periodici, che vengono aumentati in corrispondenza delle risonanze e abbassati tra di esse $\Rightarrow$ le formanti corrispondono ai picchi dell'\textbf{inviluppo} dello spettro, cioè i punti dove è concentrata l'energia.
\end{itemize}

Il sistema uditivo identifica le prime due o tre formanti per identificare i fonemi pronunciati $\Rightarrow$ sono sufficienti le prime due formanti $F_1$ e $F_2$ ai fini dell'intelligibilità (anche se con 3 formanti l'intelligibilità è migliore).

\paragraph{Parametri ideali}
\begin{itemize}
\item le prime due \textbf{formanti} $F_1$ e $F_2$
\item le \textbf{ampiezze} $Q_1$ e $Q_2$ delle prime due formanti
\end{itemize}

\subsubsection{Filtro di sintesi}
Assumendo che il sistema fonatorio cambi 50 volte al secondo, ogni 20 ms sarebbe necessario determinare l'inviluppo dallo spettro del segnale e ricavare le formanti $\Rightarrow$ la complessità di calcolo sarebbe eccessiva.

Un approccio alternativo è modellare la cavità orale come un filtro di ordine 4, chiamato \textbf{filtro di sintesi} o \textbf{filtro di predizione lineare}, la cui \href{https://it.wikipedia.org/wiki/Risposta_in_frequenza}{risposta in frequenza} ha la ``forma'' di un generico inviluppo con due picchi.

Questa forma è descritta nel dominio della frequenza da un polinomio complesso che dipende da 4 \textbf{coefficienti di predizione lineare} $\alpha_i$, i quali costituiscono i parametri da trasmettere al ricevitore: ogni 20 ms si calcolano i coefficienti di predizione lineare ottimi, tali da minimizzare localmente la distanza (nel senso euclideo) tra lo spettro del segnale e la risposta in frequenza del filtro (il calcolo è analogo alle tecniche ADPCM\footnote{Si veda la sezione~\ref{sez:tecniche_pcm_differenziali}.}, ma nel dominio della frequenza anziché nel dominio del tempo).

In pratica lo standard LPC-10 preferisce un filtro di ordine 10 (8$\div$12), che equivale a 5 risonanze, invece di un filtro di ordine 4 (2 risonanze) perché:
\begin{itemize}
\item è meglio considerare le prime tre formanti per una migliore intelligibilità;
\item la cavità nasale introduce degli zeri nella risposta in frequenza $\Rightarrow$ è necessario un ordine superiore per bilanciare il fatto che la cavità nasale è stata trascurata $\Rightarrow$ la voce è più naturale.
\end{itemize}

\paragraph{Parametri}
\begin{itemize}
\item 10 \textbf{coefficienti di predizione lineare} $\alpha_i$ ($\sim$40-45 bit)
\end{itemize}

\subsection{Codifica e decodifica}
\begin{table}[H]
 \centering
 \centerline{\begin{tabular}{|c|c|c|}
  \hline
  \textbf{Organo} & \textbf{Parametri} & \textbf{Numero di bit (ogni 20 ms)} \\
  \hline
  polmoni & gain & 7 bit \\
  \hline
  \multirow{2}{*}{laringe} & flag vocalizzato/non vocalizzato & 1 bit \\
  \cline{2-3}
  & frequenza fondamentale (se vocalizzato) & 7 bit \\
  \hline
  cavità orale & 10 coefficienti di predizione lineare & 45 bit \\
  \hline
  \multicolumn{2}{|r|}{\textbf{totale:}} & 60 bit \\
  \hline
 \end{tabular}}
 \caption{Riepilogo dei parametri del modello di produzione del segnale vocale.}
\end{table}

Il codificatore è una batteria di algoritmi di stima, seguiti dai corrispondenti quantizzatori:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\linewidth]{pic/A5/LPC-10_encoder_block_diagram}
	\caption{Diagramma a blocchi del codificatore LPC-10.}
\end{figure}

Il decodificatore produce la \textbf{voce sintetica} usando il modello di produzione del segnale vocale:
\begin{figure}
	\centering
	\includegraphics[width=0.8\linewidth]{pic/A5/LPC-10_decoder_block_diagram}
	\caption{Diagramma a blocchi del decodificatore LPC-10.}
\end{figure}
\begin{itemize}
\item suono vocalizzato: viene generato del \ul{rumore} bianco \ul{sintetico}, formato da una sequenza di numeri casuali;
\item suono non vocalizzato: una \ul{sequenza di impulsi} approssima molto bene la sequenza di sbuffi delle membrane della laringe.
\end{itemize}
\FloatBarrier

\subsection{Limiti LPC-10}
La naturalezza della codifica LPC-10 è medio-bassa (MOS $\approx$ 2,7); per migliorare la naturalezza non è sufficiente aumentare il bit rate a disposizione, ma occorre modificare l'architettura di base.

Il modello di produzione usato dalla codifica LPC-10 presenta alcuni limiti:
\begin{itemize}
\item \ul{bontà del modello}: determina il limite superiore della qualità: i parametri possono anche essere perfetti, ma non basta se il modello è scadente:
\begin{itemize}
\item[\pro] polmoni: la modellizzazione funziona bene perché sono semplici da modellare;
\item[\con] laringe: il flag vocalizzato/non vocalizzato è un'assunzione troppo semplicistica:
\begin{itemize}
\item l'intero segmento da 20 ms è forzato essere o tutto vocalizzato o tutto non vocalizzato;
\item il suono può essere una via di mezzo tra vocalizzato e non vocalizzato (ad es. voce roca);
\end{itemize}
\item[\pro] cavità orale: la modellizzazione funziona molto bene, considerando che è stato scelto un filtro di ordine 10;
\end{itemize}

\item \ul{affidabilità algoritmi di stima} (con segnali naturali):
\begin{itemize}
\item[\pro] gain: esistono algoritmi stabili;
\item[\pro] coefficienti di predizione lineare: esistono algoritmi stabili;
\item[\con] frequenza fondamentale: è difficile da stimare fedelmente con sufficiente affidabilità (ad es. musica di sottofondo, finestrino aperto\textellipsis);
\end{itemize}

\item[\pro] \ul{prestazioni dei quantizzatori}: la quantizzazione è già percettivamente trasparente.
\end{itemize}

\section{MELP}
Il \textbf{MELP} (Mixed Excitation Linear Prediction) fu il nuovo standard della NATO pubblicato a metà degli anni `90.

Il principale limite di LPC-10 era la modellizzazione della laringe: il suono può essere una via di mezzo tra vocalizzato e non vocalizzato (ad es. voce roca). Il MELP migliora la modellizzazione della laringe:
\begin{enumerate}
\item lo spettro del segnale vocale viene suddiviso sottobanda per sottobanda;
\item a ogni sottobanda viene assegnato un numero reale che identifica il tipo di componente sonora:
\begin{itemize}
\item 1 = vocalizzato (periodico)
\item 0,5 = una via di mezzo tra vocalizzato e non vocalizzato
\item 0 = non vocalizzato (rumore)
\end{itemize}
\end{enumerate}

\paragraph{Caratteristiche}
\begin{itemize}
\item bit rate: rimane lo stesso di LPC-10:
\[
R = 2400 \; \text{b/s}
\]
\item naturalezza: rimane al di sotto della toll quality, ancora troppo bassa per la telefonia commerciale:
\[
\text{MOS} \approx 3,2
\]
\end{itemize}