#!/bin/bash

#$1 = xournal 0.4.8 + http://lucaghio.webege.com/redirs/k
#$2 = krop 0.4.6 + http://lucaghio.webege.com/redirs/l

my_convert() {
  "$1" "$3.xoj" --export-pdf="$3.pdf"
  python "$2" "$3.pdf"
  mv -f "$3-cropped.pdf" "$3.pdf"
}

SAVEIFS=$IFS # http://www.cyberciti.biz/tips/handling-filenames-with-spaces-in-bash.html
IFS=$(echo -en "\n\b")

for dir in ./*/
do
  cd "$dir"
  for xoj in $(find -maxdepth 1 -type f -name "*.xoj")
  do
    my_convert "$1" "$2" "$(basename "$xoj" ".xoj")"
  done
  cd ../
done

IFS=$SAVEIFS