\chapter{Codifica di sorgente}
\begin{figure}
	\centering
	\includegraphics[width=0.66\linewidth]{pic/A3/ADA_conversion_system_block_diagram_for_audio_signals_with_compressor_it}
	\caption{Schema a blocchi di un sistema di conversione A/D e D/A con compressore.}
\end{figure}

\noindent
La voce umana naturale (fino a 12 kHz) dovrebbe essere rappresentata da un segnale digitale caratterizzato da un bit rate $R$ molto elevato:
\[
 R = N \times F_c = 16 \; \text{bit/campione} \times 24000 \; \text{Hz}  = 384000 \; \text{b/s}
\]
dove:
\begin{itemize}
\item la frequenza di campionamento $F_c$ è imposta dal teorema di Shannon:
\[
F_c > 2 \times 12000 = 24000 \; \text{Hz}
\]
\item come numero di bit $N$ si può assumere quello adottato per i CD audio:
\[
N = 16 \; \text{bit/campione}
\]
\end{itemize}

Un bit rate troppo elevato può essere un problema importante dal punto di vista di:
\begin{itemize}
\item capacità di trasmissione in rete:
\begin{itemize}
\item originariamente i modem potevano trasmettere centinaia/poche migliaia di bit al secondo;
\item oggi è importante risparmiare banda sul backbone dove passano tante telefonate;
\end{itemize}
\item potenza di elaborazione dei microprocessori.
\end{itemize}

\paragraph{Bit rate tipici di segnali digitali non compressi}
\begin{itemize}
\item musica non compressa (CD audio):
\begin{itemize}
\item mono:
\[
R=16 \; \text{bit/campione} \times 44100 \; \text{Hz} \simeq 715 \; \text{kb/s}
\]
\item stereo:
\[
R \simeq 1,4 \; \text{Mb/s}
\]
\end{itemize}
\item musica compressa (MP3):
\[
R\leq 320 \; \text{kb/s}
\]
\item video a risoluzione 640$\times$480:
\[
R=640 \; \text{px} \times 480 \; \text{px} \times 25 \; \text{fps} \times 24 \; \text{bpp} \simeq 184 \; \text{Mb/s}
\]
\item voce in banda telefonica:
\[
 R=12 \; \text{bit/campione} \times 8000 \; \text{Hz} = 96 \; \text{kb/s}
\]
\end{itemize}
\FloatBarrier

\section{Compressione}
\begin{table}
 \centering
 \centerline{\begin{tabularx}{\textwidth}{|c|c|c|X|}
  \hline
  & \begin{tabular}[c]{@{}c@{}}\textbf{rapporto di}\\\textbf{compressione}\end{tabular} & \textbf{formati comuni} & \multicolumn{1}{c|}{\textbf{applicazioni}} \\
  \hline
  \multirow{11}{*}{\textbf{lossless}} & \multirow{11}{*}{da 2 a 3 volte} & \multirow{11}{*}{RAW, FLAC, ZIP} & 
  \begin{itemize}
  \item audiofili che desiderano la massima qualità
  \item immaganizzamento dei ``master'': si evitano compressioni in cascata
  \item applicazioni particolari: applicazioni legali, ambito medico, astronomia
  \end{itemize}\\
  \hline
  \multirow{8}{*}{\textbf{lossy}} & \multirow{8}{*}{da 10 a 100 volte} & \multirow{8}{*}{MP3, JPEG, MPEG-2} & 
  \begin{itemize}
  \item memorizzazione su dischi piccoli
  \item trasmissione su canali con poca banda disponibile
  \end{itemize}\\
  \hline
 \end{tabularx}}
 \caption{Confronto tra le tecniche di compressione lossless e lossy.}
\end{table}

\noindent
Il segnale digitale in uscita dal convertitore analogico/digitale, prima di essere trasmesso o archiviato, attraversa tipicamente due blocchi:
\begin{itemize}
\item \textbf{codifica di sorgente}: si occupa di codificare il segnale digitale in maniera compatta, al fine di ridurne il bit rate, tramite tecniche di compressione che conoscono il contenuto del segnale (ad es. MP3);
\item \textbf{codifica di canale}: si occupa di codificare i bit da trasmettere in maniera robusta tramite tecniche di protezione (rilevamento e recupero) dagli errori che lavorano su sequenze di bit indipendentemente dal contenuto del segnale (ad es. codice di Hamming).
\end{itemize}

\subsection{Classificazione delle tecniche di compressione}
\begin{itemize}
\item \textbf{lossless} (senza perdite) (es. RAW, FLAC, ZIP): il segnale decompresso $y \left[ n \right]$ è identico al segnale originario $x \left[ n \right]$ $\Rightarrow$ i due segnali $y \left[ n \right]$ e $x \left[ n \right]$ sicuramente sono \ul{percettivamente indistinguibili} indipendentemente dall'ascoltatore;
\item \textbf{lossy} (con perdita) (es. MP3, JPEG, MPEG-2): il segnale decompresso $y \left[ n \right]$ è diverso dal segnale originario $x \left[ n \right]$ $\Rightarrow$ i due segnali $y[n]$ e $x[n]$:
\begin{itemize}
\item devono essere almeno \ul{percettivamente simili} secondo la statistica (cioè percepiti come simili dalla maggior parte delle persone);
\item se il bit rate è sufficientemente elevato, possono essere percepiti come indistinguibili.
\end{itemize}
\end{itemize}
\FloatBarrier

\section{Caratteristiche dei codificatori multimediali}
\begin{itemize}
\item bit rate: sezione~\ref{sez:bit_rate}
\item complessità: sezione~\ref{sez:complessita}
\item ritardo: sezione~\ref{sez:ritardo}
\item robustezza: sezione~\ref{sez:robustezza}
\item qualità: sezione~\ref{sez:qualita}
\end{itemize}

\subsection{Bit rate}
\label{sez:bit_rate}
\begin{itemize}
\item \textbf{constant bit rate} (CBR): il bit rate è costante $\Rightarrow$ la codifica è più semplice da implementare, ma meno efficiente;
\item \textbf{variable bit rate} (VBR): il bit rate è variabile nel tempo:
\begin{itemize}
\item \ul{source-driven}: il bit rate cambia in base alla variabilità del segnale (es. telefonata: voce/silenzio);
\item \ul{network-driven}: il bit rate cambia in base alla banda disponibile del canale (es. YouTube).
\end{itemize}
\end{itemize}

\subsection{Complessità}
\label{sez:complessita}
La complessità di un codificatore può essere quantificata in base a:
\begin{itemize}
\item numero di operazioni della CPU al secondo, espresso in Million Instructions Per Second (MIPS) (unità di misura indipendente dalla CPU);
\item quantità di memoria \ul{RAM} occupata durante l'esecuzione dell'algoritmo;
\item quantità di memoria \ul{ROM} richiesta per memorizzare il codice (ad es. tabelle di quantizzazione).
\end{itemize}

\subsection{Ritardo}
\label{sez:ritardo}
Il ritardo di un codificatore è dato dalla somma di due componenti:
\begin{itemize}
\item \ul{ritardo computazionale}: dipende dalle prestazioni della tecnologia (ad es. potenza della CPU) in uso, e può essere ridotto con hardware più potente;
\item \ul{ritardo algoritmico}: dipende dall'algoritmo per come è stato progettato, e non può essere ridotto se non modificando l'algoritmo (ad es. ritardo di bufferizzazione per algoritmi che lavorano su segmenti di campioni).
\end{itemize}

\subsection{Robustezza}
\label{sez:robustezza}
La robustezza può essere intesa in due modi:
\begin{itemize}
\item robustezza ai \ul{segnali di ingresso}: quanto un codificatore tarato per una specifica categoria di segnale è sensibile a un segnale diverso da quello atteso, ad esempio:
\begin{itemize}
\item musica invece di voce a un codificatore vocale;
\item voce + rumore di fondo (es. finestrino aperto);
\end{itemize}

\item robustezza agli \ul{errori di trasmissione}: anche la codifica di sorgente si preoccupa di proteggere il bitstream compresso dagli errori comprimendo in modo robusto:
\begin{itemize}
\item robustezza agli errori su singoli bit;
\item robustezza alle perdite di pacchetti (frame): ad esempio il codificatore ILBC di Skype fu progettato per Internet (rete IP con perdite).
\end{itemize}
\end{itemize}

\subsection{Qualità}
\label{sez:qualita}
L'elaborazione dei segnali multimediali guarda non tanto al rapporto segnale/rumore SNR, quanto alla percezione del segnale multimediale da parte dell'essere umano.

\begin{mdframed}[style=def]
\begin{description}
\item[percezione] l'effetto di un segnale tenuto conto di tutto il processo di elaborazione umana (cervello incluso)
\item[psico-acustica] in ambito acustico, la disciplina che studia la percezione uditiva
\end{description}
\end{mdframed}

La valutazione delle prestazioni di un algoritmo di compressione si basa principalmente sulla \textbf{valutazione percettiva}: si chiede a un numero statisticamente significativo di persone di valutare il risultato della decodifica.

\subsubsection{Voce}
La voce decodificata deve essere caratterizzata da:
\begin{itemize}
\item l'\ul{intelligibilità} (indispensabile): capire la sequenza di fonemi che viene pronunciata dall'interlocutore;
\item una \ul{sufficiente qualità} (naturalezza): capire informazioni sul parlatore (come identità, sesso, età\textellipsis).
\end{itemize}

Come misurare la naturalezza?
\begin{enumerate}
\item ad ogni ascoltatore viene fatta ascoltare la voce decodificata e viene chiesto di esprimere un giudizio soggettivo da 1 (pessimo) a 5 (eccellente);
\item si calcola il \textbf{Mean Opinion Score} (MOS) che è la media di tutti i voti.
\end{enumerate}

Il valore 4 del MOS corrisponde alla \textbf{toll} (= pedaggio) \textbf{quality}, ovvero alla qualità percepita come uguale a quella della telefonia analogica tradizionale e che la telefonia digitale deve avere come minimo perché la gente sia disposta ad acquistare il servizio. Esempi di codificatori che raggiungono la toll quality sono: PCM lineare, G.711, ADPCM G.726.

Per ottenere un risultato significativo dal punto di vista statistico, cioè affetto da un margine di errore sufficientemente piccolo ($\approx 0,1 \; \text{MOS}$), occorre che:
\begin{itemize}
\item il campione di ascoltatori sia il più possibile rappresentativo del pubblico degli utenti di telefonia:
\begin{itemize}
\item il numero di ascoltatori deve essere significativo ($\approx 40 \div 50$);
\item il campione di ascoltatori deve essere il più possibile eterogeneo (ad es. età, sesso\textellipsis);
\end{itemize}

\item il campione di stimoli sia il più possibile rappresentativo del segnale:
\begin{itemize}
\item gli ascoltatori devono essere sottoposti a un insieme statisticamente rappresentativo di tutti gli stimoli possibili (ad es. voce maschile/femminile, lingue diverse\textellipsis) ($\approx$ centinaia).
\end{itemize}
\end{itemize}

\subsubsection{Audio}
Il campione di ascoltatori è composto da esperti (golden ears) in grado di compiere un'analisi critica.

L'obiettivo è raggiungere la \textbf{trasparenza} (indistinguibilità) \textbf{percettiva}: l'ascoltatore non riesce a distinguere l'originale dal risultato della decodifica $\Rightarrow$ il campione di ascoltatori dà un voto statisticamente casuale (50\%).

\subsubsection{Misure oggettive}
I test soggettivi:
\begin{itemize}
\item sono costosi e lenti perché coinvolgono un gran numero di persone;
\item non sono completamente affidabili: sono basati sulla statistica.
\end{itemize}

Recentemente sono nate delle misure oggettive:
\begin{itemize}
\item SNR e derivati: sono tecniche rudimentali e poco utili, in quanto due segnali diversi possono suonare simili;
\item tecniche oggettive di tipo percettivo (derivate dalla psico-acustica): sono in grado di simulare come il segnale verrà percepito dall'orecchio umano, ma sono usati solo per misure relative (cioè per confronti tra due algoritmi):
\begin{itemize}
\item voce: ITU PESQ (Perceptual Evaluation of Speech Quality);
\item audio: ITU PEAQ (Perceptual Evaluation of Audio Quality).
\end{itemize}
\end{itemize}