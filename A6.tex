\chapter{Tecniche CELP}
Le codifiche parametriche ad anello aperto non sono in grado di raggiungere la toll quality: per migliorare la naturalezza non è sufficiente aumentare il bit rate a disposizione, ma occorre modificare l'architettura di base.

Negli anni `80 si studia come combinare insieme le tecniche PCM e le tecniche parametriche:
\begin{itemize}
\item codifiche PCM: seguono la \ul{forma d'onda} $\Rightarrow$ sono robuste ai segnali in ingresso;
\item codifiche parametriche: il \ul{filtro di sintesi} garantisce l'intelligibilità con pochi bit.
\end{itemize}

\section{Quantizzazione vettoriale}
Negli anni `70 viene teorizzata la \textbf{quantizzazione vettoriale} (VQ):
\begin{itemize}
\item il quantizzatore scalare di ordine $N$ trasforma 1 numero reale in uno dei $2^N$ indici, ciascuno lungo $N$ bit;
\item il quantizzatore vettoriale di ordine $N$ trasforma un vettore di $N$ numeri reali in uno dei $2^N$ indici, ciascuno lungo $N$ bit.
\end{itemize}

\paragraph{Quantizzatore vettoriale di ordine 3}
\begin{itemize}
\item un vettore contiene le tre coordinate di un punto nello spazio
\item ognuno dei $2^N=8$ possibili indici è associato a un punto nello spazio
\item dato un punto nello spazio in ingresso, il quantizzatore lo trasforma nell'indice associato al punto più vicino al punto dato
\end{itemize}

\paragraph{Vantaggi/svantaggi}
\begin{itemize}
\item[\pro] \ul{prestazioni}: è stato dimostrato che, sotto certe condizioni, un quantizzatore vettoriale di ordine $N$ produce un rapporto segnale/rumore SNR migliore di $N$ quantizzatori scalari di ordine $N$ in parallelo;
\item[\con] \ul{complessità}: cresce esponenzialmente con la dimensionalità $N$, poiché occorre calcolare la distanza euclidea in ogni dimensione.
\end{itemize}

\section{Analisi per sintesi}
\begin{figure}
	\centering
	\includegraphics[width=0.9\linewidth]{pic/A6/CELP_closed-loop_algorithm}
	\caption{Algoritmo ad anello chiuso dell'analisi per sintesi delle tecniche CELP.}
\end{figure}

\noindent
A metà anni `80 nasce l'idea di usare la quantizzazione vettoriale per fare codifica di forma d'onda a un bit rate più basso delle tecniche PCM: l'idea di base è quella di quantizzare il segnale in ingresso trasformando ogni sequenza di 40 campioni in un indice associato a una forma d'onda.

Il modello della laringe di LPC-10 viene sostituito da due cataloghi contenenti forme d'onda, ciascuna di durata 5 ms (40 campioni):
\begin{itemize}
\item \textbf{catalogo di forme d'onda}: contiene 1024 forme d'onda predefinite, ciascuna associata a un indice da 10 bit, che rappresentano la \ul{componente rumorosa};
\item \textbf{catalogo per ridondanza di lungo termine}: contiene delle forme d'onda che rappresentano la \ul{componente periodica} (viene sommata alla componente rumorosa quando il segnale ha delle componenti vocalizzate).
\end{itemize}

\paragraph{Algoritmo ad anello chiuso \textmd{(per forza bruta)}}
Per ogni sequenza $x[n]$ di 40 campioni (5 ms) del segnale in ingresso:
\begin{enumerate}
\item per ogni forma d'onda nel catalogo:
\begin{enumerate}
\item si produce la voce sintetica $\hat x [n]$ attraverso il filtro di sintesi a partire dalla forma d'onda corrente;
\item si calcola l'errore $e[n]$ tra la voce sintetica e la sequenza in ingresso;
\end{enumerate}
\item si seleziona la forma d'onda nel catalogo che genera l'errore minimo, cioè che genera la sequenza di campioni più vicina alla sequenza in ingresso;
\item si invia al decodificatore l'indice associato alla forma d'onda selezionata (insieme al gain $G$ e ai coefficienti di predizione lineare $\alpha_i$).
\end{enumerate}

\paragraph{Vantaggi/svantaggi}
\begin{itemize}
\item[\pro] \ul{bit rate}: abbatte il limite delle tecniche PCM, pur seguendo la forma d'onda (la forma d'onda ricostruita è simile alla forma d'onda originale):
\[
R \sim 8 \div 10 \; \text{kb/s}
\]
\item[\pro] \ul{naturalezza}: la tecnica di analisi per sintesi raggiunge la toll quality;
\item[\con] \ul{complessità}: è astronomica ($\div$20 MIPS) poiché occorre calcolare 1024 errori ogni 5 ms.
\end{itemize}
\FloatBarrier

\section{Standard CELP}
A partire dalla fine degli anni `80, tutti gli standard a toll quality appartengono alla famiglia \textbf{CELP} (Codebook-Excited LP).

Prima del 3G, esistevano diversi standard per la telefonia cellulare digitale commerciale:
\begin{itemize}
\item USA: erano in competizione tra loro gli standard CDMA e TDMA;
\item Europa: è stato imposto un unico standard chiamato GSM.
\end{itemize}

\begin{table}[H]
 \centering
 \centerline{\begin{tabular}{|c|c|c|c|c|}
  \hline
  & \textbf{Nome} & \textbf{Anno} & \textbf{Bit rate} & \textbf{Qualità} \\
  \hline
  \begin{tabular}[c]{@{}c@{}}\textbf{GSM-FR}\\{\small (Full Rate)}\end{tabular} & RPE-LTP & anni `80 & 13 kb/s & MOS $\approx$ 3,5\footnotemark \\
  \hline
  \begin{tabular}[c]{@{}c@{}}\textbf{GSM-EFR}\\{\small (Enhanced Full Rate)}\end{tabular} & \begin{tabular}[c]{@{}c@{}}ACELP\\\small{(Algebric)}\end{tabular} & 1995 & 12,2 kb/s & \begin{tabular}[c]{@{}c@{}}toll quality\\(MOS $\approx$ 4)\end{tabular} \\
  \hline
  \begin{tabular}[c]{@{}c@{}}\textbf{GSM-AMR}\\{\small (Adaptive Multi-Rate)}\end{tabular} & & $\sim$2000 & \begin{tabular}[c]{@{}c@{}}variabile tra 4,8\\e 12,2 kb/s\end{tabular} & toll quality \\
  \hline
 \end{tabular}}
 \caption{Standard GSM.}
\end{table}
\footnotetext{Un po' al di sotto della toll quality a causa di un piccolo fruscio tuttavia considerato all'epoca accettabile dagli utenti dei telefoni cellulari.}

\begin{table}[H]
 \centering
 \centerline{\begin{tabular}{|c|c|c|c|c|}
  \hline
  & \textbf{Nome} & \textbf{Anno} & \textbf{Bit rate} & \textbf{Applicazioni} \\
  \hline
  \textbf{G.728} & \begin{tabular}[c]{@{}c@{}}LD-CELP\\{\small (Low-Delay)}\end{tabular} & 1995 & 16 kb/s & poco usato \\
  \hline
  \textbf{G.729} & CS-ACELP & 1998 & 8 kb/s & VoIP \\
  \hline
  \textbf{G.4K} & \multicolumn{2}{|c|}{\begin{tabular}[c]{@{}c@{}}mai standardizzato\\{\small (per motivi non tecnologici di brevetto)}\end{tabular}} & 4 kb/s & \\
  \hline
 \end{tabular}}
 \caption{Standard ITU.}
\end{table}

L'arrivo del 4G ha permesso di passare dalla voce in banda telefonica alla \textbf{voce a larga banda} (wideband):
\begin{table}[H]
 \centering
 \centerline{\begin{tabular}{|c|c|c|}
  \hline
  & \textbf{voce in banda telefonica} & \textbf{voce a larga banda} \\
  \hline
  \textbf{frequenza di campionamento $F_c$} & 8000 Hz & 16000 Hz \\
  \hline
  \textbf{larghezza di banda $B$} & 300-3400 Hz & 50-7000 Hz \\
  \hline
 \end{tabular}}
 \caption{Standard ITU.}
\end{table}

La ricerca si è progressivamente spenta.

\subsection{GSM-FR}
Il \textbf{GSM-FR} fu il primo standard europeo per la telefonia cellulare digitale, quasi a toll quality, e venne pubblicato prima che lo sviluppo della tecnica CELP fosse stato portato a termine.

Un canale GSM full rate è ampio complessivamente 22,8 kb/s, ed è suddiviso in bit di voce e bit di correzione degli errori (Forward Error Correction [FEC]), usati dal decodificatore per rilevare e correggere gli errori causati dalla trasmissione su un canale radio (non è possibile reinviare il frame come nel TCP/IP).

L'ampiezza della suddivisione è stata stabilita prendendo come riferimento il canale tipico (Carriage/Interference [C/I] = 7 dB):
\begin{itemize}
\item FEC: 9,8 kb/s
\item voce: 13 kb/s suddivisi in:
\begin{itemize}
\item bit molto importanti: sono protetti dalla FEC, e se è rilevato un errore residuo l'intero frame viene buttato (ripetendo il precedente);
\item bit meno importanti: sono protetti dalla FEC, ma in caso di errori non butto il frame;
\item bit poco importanti: non sono protetti dalla FEC.
\end{itemize}
\end{itemize}

\subsection{GSM-AMR}
Il GSM-FR impone una suddivisione fissa tra voce e FEC, che è adatta al caso tipico ma non è adatta a situazioni particolari:
\begin{itemize}
\item nei pressi della cella telefonica: i bit di protezione degli errori non servono;
\item al limite del raggio d'azione della cella telefonica: il codice di correzione degli errori esce dalla sua zona operativa e provoca ancora più errori in cascata.
\end{itemize}

Il \textbf{GSM-AMR} è un algoritmo a bit rate variabile di tipo network-driven, che permette di costruire un sistema voce/FEC adattativo alle \ul{condizioni istantanee del canale}.

La suddivisione tra voce e FEC non è fissa, ma si può spostare per adattarsi alla potenza di ricezione del segnale come compromesso tra qualità della voce ed efficacia della FEC:
\begin{itemize}
\item quando il segnale è buono, il bit rate è più alto ma il segnale è meno protetto;
\item quando il segnale è cattivo, il bit rate si abbassa ma il segnale è molto protetto.
\end{itemize}

Sono previsti 8 bit rate possibili:
\begin{itemize}
\item qualità minima: voce 4,8 kb/s, FEC 18 kb/s;
\item qualità massima: voce 12,2 kb/s, FEC 9,8 kb/s (come il GSM-FR).
\end{itemize}

È meglio una voce codificata peggio ma ben protetta, o una voce codificata meglio ma soggetta a più errori?
\begin{itemize}
\item segnale buono: conviene migliorare la qualità di partenza del segnale vocale, anche se ci potrà essere qualche errore;
\item segnale cattivo: conviene proteggere molto il segnale vocale e codificare una voce di minore qualità, piuttosto che avere una voce originariamente di alta qualità ma ``bombardata'' da errori.
\end{itemize}