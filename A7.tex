\chapter{Codifica dell'audio}
\begin{mdframed}[style=def]
\begin{description}
\item[audio] l'insieme dei suoni percepibili dal sistema uditivo umano
\end{description}
\end{mdframed}

I segnali audio naturali si estendono:
\begin{itemize}
\item intensità: tra la soglia di udibilità e la soglia del dolore;
\item frequenza: tra 20 Hz a 20000 Hz.
\end{itemize}

Come codificare un generico segnale audio naturale?
\begin{itemize}
\item[\con] \ul{codifica parametrica}: non è pensabile un modello di produzione per tutti i suoni naturali possibili;
\item \ul{codifica di forma d'onda}: le tecniche PCM lavorano campione per campione:
\begin{itemize}
\item[\pro] bassissimo ritardo
\item[\pro] bassa complessità
\item[\pro] elevata qualità
\item[\con] alto bit rate
\end{itemize}
\item[\pro] \ul{codifica percettiva}: grazie a risultati di psicoacustica è possibile produrre forme d'onda differenti, ma percettivamente simili (o anche uguali).
\end{itemize}

\section{Codifica percettiva}
\subsection{Fenomeno del mascheramento simultaneo in frequenza}
Tra i tanti fenomeni di psicoacustica, spicca (ai fini della compressione) una famiglia di fenomeni, i \textbf{fenomeni di mascheramento}: alcune componenti di frequenza di un segnale complesso vengono mascherate (= sono presenti, ma non vengono percepite) da altre componenti di frequenza.

Il sistema uditivo è modellabile come un banco di filtri con 32 bande diseguali, chiamate \textbf{bande critiche}: ogni sensore (cilia) è stimolato dagli impulsi sonori entro la sua banda critica.

Data una sinusoide di frequenza $F_0$ e intensità $I_0$, si definisce \textbf{curva} (o campana) \textbf{di mascheramento} la curva al di sotto della quale altre sinusoidi non vengono percepite dal sistema uditivo umano. Questo fenomeno è dovuto al fatto che il sensore non riesce a catturare tutti gli impulsi all'interno della stessa banda critica, ma alcuni segnali, chiamati \textbf{segnali mascherati}, possono essere ``nascosti'' da un \textbf{segnale mascherante}: il segnale mascherato dovrebbe essere catturato dallo stesso sensore del segnale mascherante, ma in realtà il sensore, essendo già sovraeccitato dall'impulso mascherante, non riesce a discriminare questa informazione aggiuntiva.

Gli studi di psicoacustica hanno definito le curve di mascheramento al variare della frequenza $F_0$ e dell'intensità $I_0$: le curve di mascheramento sono strette ($\sim$100 Hz) alle basse frequenze, e diventano più ampie al crescere della frequenza a causa della sensibilità para-logaritmica del sistema uditivo.

\subsection{Compressione}
Come sfruttare il fenomeno ai fini della compressione? L'idea di base è quella di comprimere mantenendo il rumore (prodotto dal quantizzatore) sotto le curve di mascheramento impedendo quindi che sia percepito dall'orecchio umano.

Ipotizzando la sovrapposizione degli effetti, cioè la linearità del sistema:
\begin{enumerate}
\item si costruisce la \textbf{curva globale di mascheramento} come la somma delle curve di mascheramento delle singole sinusoidi;
\item si applica una tecnica PCM quantizzando in modo da produrre un rumore che non superi la curva globale di mascheramento.
\end{enumerate}

\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{pic/A7/Piecewise_quantizer_audio_encoder}
	\caption{Codificatore audio con quantizzatore a tratti.}
\end{figure}

Tipicamente il rumore prodotto da un quantizzatore è a intensità costante a tutte le frequenze (per questo motivo è detto \textbf{pavimento di rumore} o noise floor), ma la curva globale di mascheramento può essere assai variabile $\Rightarrow$ il pavimento di rumore non può superare la minima intensità della curva globale di mascheramento, anche alle frequenze dove potrebbe essere più alto. Un'ottimizzazione per ovviare a questo problema è approssimare la curva di mascheramento a gradini massimizzando il rumore a tratti:
\begin{enumerate}
\item si suddivide lo spettro del segnale in sottobande (idealmente le 32 bande critiche) tramite un \textbf{banco di filtri di analisi};
\item in ogni sottobanda, si quantizza in modo indipendente attraverso un quantizzatore uniforme con il numero di bit definito dal \textbf{modello psicoacustico}:
\begin{itemize}
\item se i quantizzatori uniformi possono usare sempre i numeri di bit indicati dal modello psicoacustico, cioè i numeri di bit minimi per mantenere il rumore al di sotto della curva globale di mascheramento, allora il segnale ricostruito sarà percettivamente trasparente perché il rumore iniettato è sempre non udibile;
\item se i quantizzatori non possono usare i numeri di bit del modello psicoacustico perché l'utente specifica un bit rate inferiore, il rumore supera la curva globale di mascheramento diventando udibile e il suono sarà percepito come distorto.
\end{itemize}
\end{enumerate}
\FloatBarrier

\section{Standard MPEG}
Negli anni `80, venne costituito il gruppo ISO Moving Picture Experts Group per lo sviluppo di standard multimediali digitali.

\subsection{MPEG-1}
Il primo standard, MPEG-1, venne sviluppato con l'obiettivo di rimpiazzare il VHS, e rese possibile codificare un flusso audio e video all'interno di un CD audio (bit rate: 1,4 Mb/s):
\begin{itemize}
\item parte di audio: 128 kb/s
\item parte di video: 1 Mb/s
\end{itemize}

MPEG-1 Audio usa la codifica percettiva con filtri di uguale banda per semplicità di calcolo, e offre tre layer:
\begin{itemize}
\item layer I: complessità minima, rapporto di compressione minimo
\item layer II
\item layer III (MP3): complessità massima, rapporto di compressione massimo (fattore 10), bit rate fino a 320 kb/s
\end{itemize}

\subsection{MPEG-2}
MPEG-2 riuscì ad aumentare la qualità del video grazie all'impiego di un supporto più grande, il DVD (bit rate: 6 Mb/s).