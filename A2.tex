\chapter{Conversione analogico/digitale}
\begin{figure}[H]
	\centering
	\includegraphics[width=0.77\linewidth]{pic/A2/ADA_conversion_system_block_diagram_for_audio_signals}
	\caption{Schema a blocchi di un sistema di conversione A/D e D/A.}
\end{figure}

\section{Campionamento}
\begin{figure}
	\centering
	\includegraphics[width=0.45\linewidth]{pic/A2/Sampled-signal}
	\caption[]{Campionamento nel dominio del tempo.\footnotemark}
\end{figure}
\footnotetext{Questa immagine è tratta da Wikimedia Commons (\href{https://commons.wikimedia.org/wiki/File:Digital.signal.discret.svg}{Digital.signal.discret.svg}), è stata realizzata da \href{https://cs.wikipedia.org/wiki/Wikipedista:Petr.adamek}{Petr Adámek}, dall'utente \href{https://commons.wikimedia.org/wiki/User:Rbj}{Rbj} e da \href{https://de.wikipedia.org/wiki/Benutzer:Wdwd}{Walter Dvorak} e si trova nel dominio pubblico.}

\noindent
Il \textbf{campionamento} di un segnale tempo-continuo $x(t)$ produce il segnale tempo-discreto $x[n]$, che è una sequenza equispaziata di campioni del segnale originario.

Il campionamento consiste nella moltiplicazione del segnale analogico $x(t)$ per un treno di impulsi (delta):
\[
 x[n]=\sum_n x \left( t \right) \delta \left( t - nT \right)
\]
\FloatBarrier

\subsection{Teorema del campionamento di Shannon}
Il \textbf{teorema del campionamento di Shannon} definisce come campionare un segnale tempo-continuo senza perdita di informazioni:
\begin{mdframed}[style=def]
Sotto certe condizioni, un segnale tempo-continuo può essere perfettamente ricostruito a partire dai suoi campioni se la frequenza di campionamento $F_c$ è maggiore del doppio della banda $F_0$ del segnale:
\[
 F_c > 2 F_0
\]
\end{mdframed}

\subsubsection{Condizione 1}
La \ul{banda} $F_0$ del segnale di partenza deve essere \ul{limitata}.

La maggioranza dei segnali utilizzati in realtà ha banda illimitata: esiste un intervallo al di fuori del quale il segnale è significativamente vicino a zero, ma non è mai identicamente nullo $\Rightarrow$ l'eliminazione delle parti ad alta frequenza porta a un'approssimazione, e il teorema di Shannon non è fisicamente realizzabile.

\subsubsection{Condizione 2}
Il segnale campionato può essere ricostruito perfettamente se e solo se come filtro interpolatore viene usato il \textbf{filtro passa-basso ideale}, con frequenza di taglio pari alla banda $F_0$, che corrisponde:
\begin{itemize}
\item nel dominio del tempo: alla convoluzione con la \ul{risposta all'impulso} del filtro (ovvero la funzione sinc):
\[
 x(t) = \sum_n x[n] * \delta \left( t - nT \right)
\]

\item nel dominio della frequenza: alla moltiplicazione con la \ul{funzione di trasferimento} del filtro:
\[
 X(f) = \frac{1}{T} \sum_n X \left( \frac{n}{T} \right) \delta \left(  f - \frac{n}{T} \right)
\]
\begin{itemize}
\item piatta nella banda del segnale (non distorcente);
\item a pendenza infinita in corrispondenza della frequenza di taglio;
\item nulla al di fuori della banda del segnale.
\end{itemize}
\end{itemize}

Anche in questo caso il filtro ideale non è fisicamente realizzabile, e i filtri reali introducono approssimazioni:
\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\linewidth]{pic/A2/Raised-cosine_filter}
	\caption[]{Confronto tra il filtro ideale (blu) e alcuni filtri reali.\footnotemark}
\end{figure}
\footnotetext{Questa immagine è tratta da Wikimedia Commons (\href{https://commons.wikimedia.org/wiki/File:Raised-cosine_filter.svg}{Raised-cosine filter.svg}), è stata realizzata dall'utente \href{https://en.wikipedia.org/wiki/User:Oli_Filth}{Oli Filth}, dall'utente \href{https://en.wikipedia.org/wiki/User:Krishnavedala}{Krishnavedala} e da \href{https://en.wikipedia.org/wiki/User:Edgar.bonet}{Edgar Bonet}, ed è concessa sotto la \href{http://creativecommons.org/licenses/by-sa/3.0/deed.it}{licenza Creative Commons Attribuzione - Condividi allo stesso modo 3.0 Unported}.}

\subsection{Diagramma di uguale intensità sonora}
\begin{figure}
	\centering
	\includegraphics[width=0.85\linewidth]{pic/A2/FletcherMunson_ELC}
	\caption[]{Diagramma di uguale intensità sonora.\footnotemark}
\end{figure}
\footnotetext{Questa immagine è tratta da Wikimedia Commons (\href{https://commons.wikimedia.org/wiki/File:FletcherMunson_ELC.svg}{FletcherMunson ELC.svg}), è stata realizzata da \href{https://en.wikipedia.org/wiki/User:Oarih}{Oarih Ropshkow} ed è concessa sotto la \href{http://creativecommons.org/licenses/by-sa/3.0/deed.it}{licenza Creative Commons Attribuzione - Condividi allo stesso modo 3.0 Unported}.}

\begin{mdframed}[style=def]
\begin{description}
 \item[suono] onde trasversali di pressione che si propagano in un mezzo (tipicamente l'aria)
 \item[audio] l'insieme dei suoni percepibili dal sistema uditivo umano
\end{description}
\end{mdframed}

\noindent
L'\textbf{audio} è caratterizzato da intensità e frequenza.\footnote{I valori di SPL e di frequenza riportati di seguito sono convenzionali, ma dipendono in realtà da fattori legati alla persona come l'età, la salute, ecc.}

\subsubsection{Intensità (dB)}
La misura dell'intensità è il \textbf{Sound Pressure Level} (SPL):
\[
 \text{SPL} = 10 \log_{10}{\frac{P}{P_0}} \; \text{dB}
\]
dove $P_0$ è la pressione della sinusoide minimamente udibile alla frequenza di riferimento (1 kHz).

Il suono udibile è compreso tra la \textbf{soglia di udibilità} e la \textbf{soglia del dolore}:
\begin{itemize}
\item 0 dB = soglia di udibilità: suoni al di sotto di questa soglia non sono udibili dal sistema uditivo umano;
\item 100 dB = soglia del danno irreversibile: suoni al di sopra di questa soglia possono ridurre la capacità uditiva in maniera permanente;
\item 120 dB = soglia del dolore fisico: suoni al di sopra di questa soglia provocano danni fisici al timpano.
\end{itemize}

\subsubsection{Frequenza (Hz)}
Il suono udibile è compreso tra 20 Hz e 20 kHz, per un'ampiezza pari a 10 ottave\footnote{Si raddoppia circa 10 volte:
\[
20 \rightarrow 40 \rightarrow 80 \rightarrow 160 \rightarrow 320 \rightarrow 640 \rightarrow 1080 \rightarrow 2160 \rightarrow 4320 \rightarrow 8620 \rightarrow 17740
\]

Per confronto, il sistema visivo si limita a un intervallo di frequenze ampio appena 1 ottava.}. La curva di udibilità è \ul{fortemente non lineare}:
\begin{itemize}
\item l'intervallo di frequenze tra 1 kHz e 4 kHz comprende i suoni a cui il sistema uditivo è maggiormente sensibile (soglia di udibilità molto bassa);
\item a frequenze molto basse o molto alte, possono essere sentiti solo suoni a intensità molto alte (soglia di udibilità molto alta).
\end{itemize}

\subsection{Voce}
La voce umana naturale è compresa:
\begin{itemize}
\item intensità: entro una dinamica ampia 60 dB (dal bisbiglio all'urlo);
\item frequenza: nell'intervallo da 20 Hz a 12 kHz.
\end{itemize}

Tuttavia per la voce trasmessa via telefono si è visto empiricamente che è sufficiente una banda compresa tra 300 e 3400 Hz, detta \textbf{banda telefonica}, in modo da garantire:
\begin{itemize}
\item l'\ul{intelligibilità} (indispensabile): capire la sequenza di fonemi che viene pronunciata dall'interlocutore;
\item una \ul{sufficiente qualità} (naturalezza): capire informazioni sul parlatore (come identità, sesso, età\textellipsis).
\end{itemize}

La voce in banda telefonica (narrowband voice) deve essere campionata a una frequenza maggiore della minima frequenza di campionamento imposta dal teorema di Shannon $\Rightarrow$ viene campionata alla frequenza di 8 kHz per tenere conto delle non idealità dei filtri.

Oggigiorno nuove tecnologie (ad es. VoIP) rendono possibile la voce a banda larga (wideband):
\begin{itemize}
\item larghezza di banda = 50-7000 Hz
\item frequenza di campionamento = 16 kHz
\end{itemize}
\FloatBarrier

\section{Quantizzazione}
La \textbf{quantizzazione} permette di trasformare un segnale tempo-discreto $x[n]$ in un segnale digitale (o numerico) $\hat{x}[n]$.

La \textbf{zona operativa} (o dinamica, o fondo scala) $X_m$ è l'intervallo di valori che ogni campione può assumere sulla scala reale. Dati $N$ bit:
\begin{enumerate}
\item la zona operativa viene suddivisa in $2^N-1$ intervalli, chiamati \textbf{gradini} (o step) \textbf{di quantizzazione};
\item ogni campione viene mappato su uno dei $2^N$ valori possibili, e in particolare al più vicino (secondo la distanza euclidea).
\end{enumerate}

L'operazione di quantizzazione introduce un errore \ul{irreversibile}, chiamato \textbf{errore} (o rumore) \textbf{di quantizzazione} $e \left[ n \right]$, pari alla differenza fra un campione reale $x \left[ n \right]$ e la sua versione quantizzata $\hat x \left[ n \right]$:
\[
 \left| e \left[ n \right] \right| = \left| \hat{x} \left[ n \right] - x \left[ n \right] \right| \leq \frac{\Delta}{2}
\]
dove $\Delta$ è l'ampiezza del gradino di quantizzazione. Nel quantizzatore uniforme\footnote{Si rimanda alla sezione~\ref{sez:quantizzatore_uniforme}.}, tutti i gradini di quantizzazione hanno ampiezza costante $\Delta= \frac{X_m}{2^N}$.

Un campione può assumere tipicamente tutti i valori sulla scala reale $\Rightarrow$ la \textbf{zona di saturazione} (o overload) comprende i valori al di fuori della zona operativa, in cui l'errore di quantizzazione può essere potenzialmente infinito.

\subsection{Progetto di un quantizzatore}
\subsubsection{Numero di bit per campione}
Il numero $N$ di bit per campione dipende da:
\begin{itemize}
\item ampiezza $X_m$ della zona operativa: a parità di qualità, il numero di livelli necessario cresce con l'ampiezza della zona operativa;
\item errore di quantizzazione $e[n]$: a parità di ampiezza della zona operativa, il numero di livelli necessario cresce con la qualità (prestazioni) della quantizzazione.
\end{itemize}

\paragraph{Valori tipici}
\begin{itemize}
\item CD audio: 16 bit/campione
\item voce telefonica: 12 bit/campione (minore qualità della musica + minore potenza del segnale)
\item immagini in scala di grigi: 8 bpp (bit/pixel)
\item immagini a colori: 24 bpp
\end{itemize}

\subsubsection{Ampiezza della zona operativa}
\label{sez:ampiezza_zona_operativa}
A parità di numero $N$ di bit, la scelta dell'ampiezza $X_m$ della zona operativa deriva dal compromesso tra:
\begin{itemize}
\item \ul{zona stretta}: più la zona operativa è stretta e i livelli sono fitti, più l'errore di quantizzazione è basso e le prestazioni del quantizzatore sono alte;
\item \ul{zona ampia}: la zona operativa deve includere i valori a probabilità più alta in modo da minimizzare la \textbf{probabilità di overload}, ossia la percentuale dei campioni il cui valore cade al di fuori della zona operativa.
\end{itemize}

Assumendo una distribuzione di probabilità gaussiana, si è visto empiricamente che la scelta di una zona operativa con un'ampiezza $X_m$ pari a $4 \sigma$ comporta una percentuale di overhead pari allo 0,069\% circa.

\subsection{Rapporto segnale/rumore}
La qualità del segnale quantizzato è espressa in termini del \textbf{rapporto segnale/rumore} SNR, definito come il rapporto tra la potenza $\sigma_x^2$ del segnale non ancora quantizzato $x \left[ n \right]$ e la potenza $\sigma_e^2$ dell'errore di quantizzazione $e \left[ n \right]$:
\[
 \text{SNR} = 10 \log_{10}{\frac{\sigma_x^2}{\sigma_e^2}} \; \text{dB}
\]
dove la potenza $\sigma_x^2$ di un segnale $x(t)$ avente una funzione densità di probabilità $\text{PDF}_x \left( t \right)$ è:
\[
 \sigma_x^2 = \int_{-\infty}^{+ \infty} x^2 \left( t \right) \cdot \text{PDF}_x \left( t \right) dt
\]

\subsection{Quantizzatore ottimo}
\label{sez:quantizzatore_ottimo}
Un quantizzatore si dice \textbf{ottimo} per un certo segnale se la sua distribuzione di livelli è tale che:
\begin{itemize}
\item tutti i livelli di quantizzazione vengono utilizzati con pari probabilità, cioè nessun livello è utilizzato più di altri;
\item l'energia $\sigma_e^2$ dell'errore di quantizzazione $e \left[ n \right]$ viene minimizzata;
\item il rapporto segnale/rumore SNR viene massimizzato.
\end{itemize}

Il quantizzatore ottimo si ottiene facendo ``combaciare'' la distribuzione dei livelli e la funzione PDF del segnale. Il \textbf{teorema di Max-Lloyd} permette di ricavare la distribuzione ottima di livelli a partire dall'espressione analitica della funzione PDF del segnale.

Il quantizzatore uniforme è un quantizzatore ottimo per segnali distribuiti uniformemente sulla zona operativa, ma i segnali audio tipicamente hanno una distribuzione di probabilità non uniforme.